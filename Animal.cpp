﻿#include <iostream>

class Animal
{
private:
    std::string woofing;

public:
    Animal()
    {

    }

    Animal(std::string wasd)
    {
        this->woofing = wasd;
    }

    void Voice()
    {
        std::cout << woofing<<'\n';
    }
};

class Dog : public Animal
{
private:
    
public:
    Dog() : Animal ("Woof!")
    {
        
    }
};

class Cat : public Animal
{
private:

public:
    Cat() : Animal("Meow!")
    {

    }
};

class Cow : public Animal
{
private:

public:
    Cow() : Animal("Moo!")
    {

    }
};



int main()
{
    setlocale(LC_ALL, "rus");

    Cat* cat = new Cat;
    //cat->Voice();

    Dog* dog = new Dog;
    //dog->Voice();

    Cow* cow = new Cow;
    //cow->Voice();
        
    
    

    const int LENGTH = 3;
    
    Animal arr[LENGTH]{*dog, *cat, *cow};
    
    for (int i = 0; i < LENGTH; i++)
    {
        arr[i].Voice();
        
    }
    
    

}

